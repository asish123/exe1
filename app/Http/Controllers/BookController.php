<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BookValidationRequest;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $id = Auth::id();
        //$books = Book::all();
        if (Gate::denies('manager')) {
            $boss = DB::table('employees')->where('employee',$id)->first();
            $id = $boss->manager;
        }
        $user = User::find($id);
        $books = $user->books;
        return view('books.index', compact('books'));
 
       // $books = User::find($id)->books;
        //return view('books.index',['books'=>$books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create books..");
        }
        return view('books.create');

 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookValidationRequest $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }
        
        $this->validate($request, [
            'title' => 'required|unique|max:255',
            'body' => 'required',
        ]);
    
        $validated = $request->validated();

        $books = new Book();
        $id = Auth::id();
        $books->title = $request->title;
        $books->user_id = $id;
        $books->author = $request->author;
        $books -> status = 0;
        $books-> save();
        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }
        return view('books.edit',compact('book'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
   {
    //only if this todo belongs to user
    $book = Book::findOrFail($id);
   //employees are not allowed to change the title 
    if (Gate::denies('manager')) {
        if ($request->has('title'))
               abort(403,"You are not allowed to edit todos..");
    }   
    //make sure the todo belongs to the logged in user
    if(!$book->user->id == Auth::id()) return(redirect('books'));

    //test if title is dirty
   
    $book->update($request->except(['_token']));

    if($request->ajax()){
        return Response::json(array('result'=>'sucess', 'status'=>$request->status),200); 
    }
    return redirect('books');
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }
        return redirect('books');
    }
}
