<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
   protected $fillable =[
      'title', 'status'
   ];
   public function user(){
       return $this->belongsTo('App\User');
   }
}
