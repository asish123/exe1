<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books') ->insert(
            [   
                [
                    'title' => 'Kitzur Toldot Haenushut',
                    'author' => 'Yuval Noh Harri',
                    'user_id' => '1',
                    'created_at' => date('Y-m-d G:i:s'),
                    
                    
                ],
                [
                    'title' => 'Hhistoria Shel Hamachar',
                    'author' => 'Yuval Noh Harri',
                    'user_id' => '1',
                    'created_at' => date('Y-m-d G:i:s'),
                  
                ],
                [
                    'title' => 'Misala Achat Iamina',
                    'author' => 'Eshkol Nevo',
                    'user_id' => '2',
                    'created_at' => date('Y-m-d G:i:s'),
                
                ],
                [
                    'title' => 'Noyland',
                    'author' => 'Eshkol Nevo',
                    'user_id' => '2',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'title' => 'Arbaa Batim Vegagoa',
                    'author' => 'Eshkol Nevo',
                    'user_id' => '2',
                    'created_at' => date('Y-m-d G:i:s'),
                ]
            ]);
}}