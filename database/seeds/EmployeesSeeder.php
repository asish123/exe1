<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name'=> 'gal',
                'email'=>'gal@gmail.com',
                'password' => Hash::make('1234567'),
                'created_at' => date('y-m-d G:i:s'),
                ],
                [
                'name'=> 'reute',
                'email'=>'reute@jack.com',
                'password' => Hash::make('123456789'),
                'created_at' => date('y-m-d G:i:s'),
                ],
            ]
    );
    }
}
