@extends('layouts.app')
@section('content')

<h1> Edit your book list </h1>
<form method = 'post' action = "{{action('BookController@update',$book->id)}}">
@csrf
@method('PATCH')

<div class="form-group">
<label for = "title">Update</label>
<input type = "text" class = "form-control" name= "title" value="{{$book->title}}">

</div>

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Save">
</div>

</form>

<form method = 'post' action = "{{action('BookController@destroy',$book->id)}}">
@csrf
@method('DELETE')

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Delete">
</div>

</form>
@endsection