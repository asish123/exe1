@extends('layouts.app')
@section('content')


<!DOCTYPE html>
<html>
<head><h3>This is your Book List</h3></head>
<body>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<table>
    <tr>
     <th>ID</th>
     <th>Book Title</th> 
     <th>Book Author</th>
     <th>User_id </th>
     <th>checkbox </th>
    </tr>

    @foreach($books as $book)
    <tr>
        <td>{{$book->id}}</td>
        <td> @can('manager') <a href = "{{route('books.edit',$book->id)}}"> @endcan {{$book->title}} </a> </td>
        <td>{{$book->author}}</td>
        <td>{{$book->user_id}}</td>
        <td>  @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif </td>
   </tr>
   
   @endforeach

</table>
</body>
</html>
@can('manager')

<a href ="{{route('books.create')}}"> Create a new book </a>
@endcan

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json' , 
                   type:  'PUT' , //אומר לאיזה פונ' זה יגיע 
                   contentType: 'aplication/json',
                   data: JSON.stringify({'status':event.target.checked,_token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script> 

@endsection