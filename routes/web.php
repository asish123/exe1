<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
#תרגול בכיתה
Route::get('hello', function () {
    return 'hello world';
});
Route::get('/studend/{id?}', function ($id= 'no student provided') {
    return 'hello student '.$id;
}) -> name('student');


#תרגיל מס 2 
Route::get('/customer/{num?}', function ($num =null) {
    if ($num ==null){
       return view( 'nocustomer');
    }
    else
        return view('customrnum', ['num'=>$num]);
    
}) -> name('customer');

#מונע מאנשים שאין להם סיסמא ומשתמש להיכנס לעמוד 
Route::resource('books', 'BookController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
